
### 이 문서는 `SAMSUNG HEALTH`와의 데이터 동기화 관련 문서입니다.

## Health 동기화 프로세스
```mermaid
sequenceDiagram
    participant fe as Front/End
    participant be as Back/End
    participant hd as Health Data
    participant db as Database ORACLE

    fe ->> be: 동기화 요청
    be ->> be: Access Token 확인
    alt 토큰이 없을 경우
        be -->> fe: 삼성헬스 로그인 페이지 호출
    end
    be ->> db: User정보 조회 요청
    db -->> be: 조회 응답
    be ->> hd: change API 요청
    Note over be,hd: GET /health/v1/users/me/{data_type}/_changes
    Note over be,hd: paramater : changed_after	
    hd -->> be: 응답
    Note over be,hd: datauuid, update_time, action 응답
    alt action = 0일 경우
        be ->> db: Delete 요청
    else action = 1일경우
        alt DB에 데이터가 없을 경우
            be ->> db: Insert 요청
        else DB에 데이터가 있을 경우
            be ->> db: Update 요청
        end
    end
```

## byte[] 값 변환
* Response 응답값 중 byte[] 형태로 넘어오는 데이터 처리는 아래 모듈을 사용하여 변환한다.
```bash
public List<LiveData> getLiveData(byte[] zip) throws JsonSyntaxException, IOException {
    // decompress ZIP
    List<LiveData> liveDataList = HealthDataUtil.getLiveData(zip);
    return liveDataList;
}

// HealthDataUtil
public class LiveData {
    private long start_time;
    private float heart_rate;
    private float cadence;
    private float count;
    private float power;
    private float speed;
}

public static ArrayList<LiveData> getLiveData(byte[] zip) throws IOException, JsonSyntaxException {
    final int BUFFER_SIZE = 1024*1000;
    ByteArrayInputStream bis = new ByteArrayInputStream(zip);
    GZIPInputStream gis = new GZIPInputStream(bis, BUFFER_SIZE);
    StringBuilder jsonBuilder = new StringBuilder();
    byte[] data = new byte[BUFFER_SIZE];
    int bytesRead;
    while ((bytesRead = gis.read(data)) != -1) {
        jsonBuilder.append(new String(data, 0, bytesRead));
    }
    gis.close();
    bis.close();
    ArrayList<LiveData> liveData = new ArrayList<LiveData>();
    Gson gson = new Gson();
    liveData = gson.fromJson(jsonBuilder.toString(), new TypeToken<List<LiveData>>() {}.getType());
    return liveData;
}
```

## RESTful API를 위한 기본 세팅 정보 및 개발 가이드 
1. sdkVersion : 삼성 Health sdk version 정보
2. serviceInfo : 데이터 동기화 (dataSyncNotiApi)를 사용하기 위한 Oject 정보
3. resourceInfo : 그외 정보들 ( Token 등 )

```json
{
    "sdkVersion": 1,    
    "serviceInfo": {    
        "serviceName":"Hydrohealth",    //servicename
        "serviceVersion":1, //serviceversion
        "accessTokenApi":{  //토큰을 받기 위한 API 
            "prd":"https://hydrohealth.com/getToken/v1",        // 운영URL정보 
            "stg":"https://stg.hydrohealth.com/getToken/v1",    // 품질(stage)URL정보
            "dev":"https://dev.hydrohealth.com/getToken/v1"     // 개발URL정보
        }, 
        "serviceDataApi":{  //타일생성을위한API
            "prd":"https://hydrohealth.com/tile/v1", 
            "stg":"https://stg.hydrohealth.com/tile/v1", 
            "dev":"https://dev.hydrohealth.com/tile/v1" 
        }, 
        "dataSyncNotiApi":{ //데이터동기화를위한API
            "prd":"https://hydrohealth.com/sync/v1", 
            "stg":"https://stg.hydrohealth.com/sync/v1", 
            "dev":"https://dev.hydrohealth.com/sync/v1" 
        }, 
        "deactivationUrl":{ //타일제거를위한API
            "prd":"https://hydrohealth.com/deactivate/v1", 
            "stg":"https://stg.hydrohealth.com/deactivate/v1", 
            "dev":"https://dev.hydrohealth.com/deactivate/v1" 
        }, 
        "dataTypes":[   //데이터동기화의대상이되는data-type-List정보 
            "com.samsung.health.blood_glucose", 
            "com.samsung.health.sleep" 
        ] 
    },
    "resourceInfo": {
        "templateType": 1,
        "contentUrl": string,
        "data": {
            "icon": string,
            "title": string,
            "mainValue": string,
            "mainGoal": string,
            "mainUnit": string
            .......
        }
    }
}
```

### 데이터 동기화 DataSyncNotiApi 흐름도

```mermaid
sequenceDiagram
    participant usr as User
    participant sha as Samsung Health APP
    participant sdb as Samsung Health DB
    participant hc as Health Care
    participant hcdb as Health Care DB
    
    usr ->> sha : 정보 등록/변경 요청
    sha -->> usr : 응답
    usr ->> sha : 동기화 요청
    sha ->> sdb : 데이터 저장/변경 요청
    sdb -->> usr : 동기화 응답
    usr ->> sha : App 접근 -> Health care 타일 요청
    sha ->> sdb : 변경 이력 요청
    sdb -->> sha : 변경 이력 응답
    alt 변경 이력이 있을 경우
        sha ->> hc : dataSyncNotiApi URL 호출
        Note over sha,hc : RequestBody에 dataTypes값으로 반환
        loop dataTypes 갯수만큼
            hc ->> sha : change URL 호출
            sha -->> hc : response값 응답
            alt response action = 0
                hc ->> hcdb : 데이터 Delete 요청
            else response action = 1
                hc ->> hcdb : READ API 호출 ( UPDATE or INSERT )
            end
        end
    end 
```