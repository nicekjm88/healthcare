# 이 문서는 `SAMSUNG HEALTH`로 데이터를 보내는 내용에 관련한 문서 입니다.

## 데이터 흐름도

### TOMS 취식 이력 정보 동기화
```mermaid
sequenceDiagram
    participant hcs as Health Care Server
    participant hcdb as Health Care DB
    participant tom as Toms
    participant sh as Samsung Health
    
    hcs ->> hcdb : Seq Max값 요청
    Note over hcdb: TB > HC_TOMS_INTK
    hcdb -->> hcs : 응답
    Note over hcs,hcdb: SUM_SEQNO, SUM_GROUP_NO
    hcs ->> tom : 취식이력 조회
    Note over tom: IF_INF_HC_GET_ORD_LIST_EXT 호출
    tom -->> hcs : List 응답
    loop List 갯수만큼
        hcs ->> hcdb : TOMS 취식 이력 등록
        Note over hcdb : HC_TOMS_INTK 테이블 Insert
        hcs ->> hcdb : 취식이력 등록 값 Update
        Note over hcdb : HC_TOMS_INTK 테이블 Update
        Note over hcdb : HALL_NO, MENU_COURSE_TYPE, MENU_CODE
        hcs ->> hcdb : 게시메뉴별 취식이력 등록
        Note over hcdb : HC_USER_CNTRL_LST 테이블 Insert
    end
```

### Samsung Health로 취식이력 데이터 전송
```mermaid
sequenceDiagram
    participant hcs as Health Care Server
    participant hcdb as Health Care DB
    participant tom as Toms
    participant sh as Samsung Health

    hcs ->> hcdb : 삼성헬스로 보내야하는 대상 조회
    Note over hcdb : [HC_USER_CNTRL_LST], [HC_USER] - JOIN
    hcdb -->> hcs : User List 반환
    loop list 갯수 만큼
        hcs ->> hcdb : 취식이력 대상 음식 정보 조회
        Note over hcdb: [HC_USER_CNTRL_LST][HC_USER][HALL_MENU_DISP][SH_FOOD_INFO] JOIN
        hcdb -->> hcs : 음식정보 List 반환
        loop list 갯수만큼
            hcs ->> sh : 취식이력 정보 write API호출
            Note over hcs,sh : /health/v1/users/me/com.samsung.health.food_info URL로 write 호출한다.
            sh -->> hcs : 결과값 응답
            Note over hcs : HttpStatus Response
            alt code = 200
                hcs ->> hcdb : 삼성헬스로 보낸 취식이력 데이터 저장
                Note over hcs,hcdb : [SH_FOOD_INFO] 테이블에 Insert
            else
                hcs ->> hcs : 프로세스 Out
            end
        end
    end
    
    
    
```    