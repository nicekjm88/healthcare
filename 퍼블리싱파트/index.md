삼성 웰스토리 웹퍼블리싱 가이드라인
=====================================

### 언어
한국어

### 호환성
Mobile : 모바일은 갤럭시8(Android Ver4.4 이상) / 아이폰6 기준으로 진행(IOS 7 이상)

### 파일 및 폴더 구조
이 구조는 가장 일반적인 형태이며, 프로젝트의 규모에 따라 폴더 뎁스를
가감할 수 있다. \
 각 카테고리 명 및 HTML 파일은 [폴더 및 파일 네이밍
규칙](nameing_default.html)을 준수하도록 한다.\

-   welhc_front/
    -   WebContent
        -   fonts/ 웹폰트(본고딕)
        -   css/
            - vondors/
                - jquery.mobile.datepicker.css
                - jquery.mobile.datepicker.theme.css
                - swiper.min.css
            -   common.css 리셋 + 전역 스타일, 공통(버튼, 탭메뉴, 박스, 불릿, 서식 등) 스타일 정의
            -   layout.css 전체 공통 레이아웃(헤더, 푸터, 사이드 바 등) 템플릿
            -   main.css 메인페이지
            -   sub.css 서브페이지[김지만]
            -   sub.scss 서브페이지(css preprocessor)
            -   sub.css.map(css with scss mapping)
            -   sub02.css 서브페이지[김동영]
            -   sub02.scss 서브페이지(css preprocessor)
            -   sub02.css.map(css with scss mapping)
            -   sub03.css 서브페이지[한슬기]
            -   mixin.scss 믹스인
        -   images/
            -   common/ 레이아웃(헤더, 푸터, 사이드 바 등), 공통(버튼, 탭메뉴, 박스, 불릿, 서식 등) 관련 이미지
            -   logo/ MEAL파트 벤더로고모음
            -   main/ 메인페이지
            -   sub서브페이지
        -   js/
            -   common.js 공통 UI 콘트롤
            -   
                        


-   \_include/ 전체 공통영역 jsp 인크루드 폴더
-   meal/ 식사

COPYRIGHT (C) 2018 EMOTION GLOBAL INC. ALL RIGHTS RESERVED.\

